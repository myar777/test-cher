# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import GameResults

@admin.register(GameResults)
class GameResultsAdmin(admin.ModelAdmin):
    pass
