# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import string
import webcolors

from django.shortcuts import render
from django.views.generic import TemplateView

from .forms import ParametersGameForm

LETTERS = string.ascii_uppercase
COLORS = webcolors.css3_names_to_hex.items()
DATA = {LETTERS[i]: COLORS[i] for i in range(len(LETTERS))}


class GameView(TemplateView):

    template_name = "core/game.html"
    form_class = ParametersGameForm
    rez = None

    def get_context_data(self, *args, **kwargs):
        context = super(GameView, self).get_context_data(**kwargs)
        context.update({'form': self.form,
                        'title_page': self.title_page,
                        'rez': self.rez})
        return context

    def build_form(self, **kwargs):
        data = None
        if len(self.request.POST):
            data = self.request.POST
        return self.form_class(data, **kwargs)

    def play_game(self, data):
        players = int(data.get('am_players'))
        cards = data.get('cards')
        mape = list(data.get('characters')[0])
        win = None
        card = data.get('am_cards')
        for i in range(0, card):
            coef = (i + 1) % players
            player = 'player%s' % coef if coef != 0 else 'player%s' % players
            for j in range(0, len(cards[i])):
                if cards[i][j] in mape:
                    mape.remove(cards[i][j])
                else:
                    win = player
                    card = i + 1
                    break
            if win:
                break
        if win:
            return 'Player "%s" won after %s cards.' % (win, card)
        else:
            return 'No player won after %s cards.' % card

    def get(self, *args, **kwargs):
        self.title_page = 'Start game'
        self.form = self.build_form()
        return render(self.request,
                      self.template_name,
                      self.get_context_data())

    def post(self, *args, **kwargs):
        self.title_page = 'Results game'
        self.form = self.build_form()

        if self.form.is_valid():
            data = self.form.cleaned_data
            gr = self.form.save()
            self.rez = self.play_game(data)
            gr.rezults = self.rez
            gr.save()
        return render(self.request,
                      self.template_name,
                      self.get_context_data())
