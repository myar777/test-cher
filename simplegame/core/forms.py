
# -*- coding: utf-8 -*-

from django import forms
from django.core import validators

from .models import GameResults


class CommaSeparatedCharField(forms.Field):

    def to_python(self, value):
        if value in validators.EMPTY_VALUES:
            return []
        value = [item.strip() for item in value.split(',') if item.strip()]
        return value

    def clean(self, value):
        value = self.to_python(value)
        self.validate(value)
        self.run_validators(value)
        return value


class ParametersGameForm(forms.ModelForm):
    am_players = forms.IntegerField(label='Amount of players', min_value=1,
                                    max_value=4)
    am_squares = forms.IntegerField(label='Amount of squares', min_value=1,
                                    max_value=79)
    am_cards = forms.IntegerField(label='Amount of cards', min_value=1,
                                  max_value=200)
    characters = CommaSeparatedCharField(label='Characters')
    cards = CommaSeparatedCharField(label='Cards')

    class Meta:
        model = GameResults
        exclude = ['rezults', ]
