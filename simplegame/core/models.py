# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class GameResults(models.Model):
    am_players = models.IntegerField(u'Amount of players')
    am_squares = models.IntegerField(u'Amount of squares')
    am_cards = models.IntegerField(u'Amount of cards')
    characters = models.TextField(u'Characters')
    cards = models.TextField(u'Cards')
    rezults = models.TextField(u'Rezults', null=True, blank=True)
